import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify, {
  theme: {
    primary: '#0d447f',
    secondary: '#55524e',
    accent: '#8acaf8',
    error: '#F44336',
    success: '#4CAF50',
  },
  iconfont: 'mdi'
});
