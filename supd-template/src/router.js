import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [

        {
            path: '/',
            name: 'home',
            component: () => import('./views/Home.vue')
        },
        {
            path: '/guide',
            name: 'guide',
            component: () => import('./views/Guide.vue')
        },
        // Tasks
        {
            path: '/tasks/home-1',
            name: 'home-1',
            component: () => import('./views/tasks/Home-1.vue')
        },
        {
            path: '/tasks/home-2',
            name: 'home-2',
            component: () => import('./views/tasks/Home-2.vue')
        },
        {
            path: '/tasks/task-1',
            name: 'task-1',
            component: () => import('./views/tasks/Task-1.vue')
        },
        {
            path: '/tasks/task-2',
            name: 'task-2',
            component: () => import('./views/tasks/Task-2.vue')
        },
        {
            path: '/tasks/task-3',
            name: 'task-3',
            component: () => import('./views/tasks/Task-3.vue')
        },
        {
            path: '/tasks/task-4',
            name: 'task-4',
            component: () => import('./views/tasks/Task-4.vue')
        },

        // Inbox
        {
            path: '/inbox/form-1',
            name: 'form-1',
            component: () => import('./views/inbox/Form-1.vue')
        },
        {
            path: '/inbox/form-2',
            name: 'form-2',
            component: () => import('./views/inbox/Form-2.vue')
        },

        //Journals
        {
            path: '/journals/overview',
            name: 'Journal overview',
            component: () => import('./views/journals/Overview.vue')
        },
        {
            path: '/journals/notifications',
            name: 'Journal notifications',
            component: () => import('./views/journals/Notifications.vue')
        },
        {
            path: '/journals/mytasks',
            name: 'My Tasks',
            component: () => import('./views/journals/MyTasks.vue')
        },
        {
            path: '/journals/mydocuments',
            name: 'My Documents',
            component: () => import('./views/journals/MyDocuments.vue')
        },
        {
            path: '/journals/taskRedistribution',
            name: 'Task Redistribution',
            component: () => import('./views/journals/TaskRedistribution.vue')
        },
        {
            path: '/journals/trademarkRequests',
            name: 'Trademark Requests',
            component: () => import('./views/journals/TrademarkRequests.vue')
        },
        {
            path: '/journals/AoORequests',
            name: 'AoO Requests',
            component: () => import('./views/journals/AoORequests.vue')
        },
        {
            path: '/journals/trademarkRegistration',
            name: 'Trademark Registration',
            component: () => import('./views/journals/TrademarkRegistration.vue')
        },
        {
            path: '/journals/AoORegistration',
            name: 'AoO Registration',
            component: () => import('./views/journals/AoORegistration.vue')
        },
        {
            path: '/journals/audit',
            name: 'Audit',
            component: () => import('./views/journals/Audit.vue')
        },
        {
            path: '/journals/assistantsAndAlternates',
            name: 'Assistants and Alternates',
            component: () => import('./views/journals/AssistantsAndAlternates.vue')
        },
        {
            path: '/journals/instructions',
            name: 'Instructions',
            component: () => import('./views/journals/Instructions.vue')
        },

        //Outbox
        {
            path: '/outbox/registration',
            name: 'Outbox tasks',
            component: () => import('./views/outbox/Tasks.vue')
        },
        {
            path: '/outbox/project',
            name: 'Outbox draft docs',
            component: () => import('./views/outbox/Project.vue')
        },
        {
            path: '/dictionary/outboxTemplate',
            name: 'Outbox template list',
            component: () => import('./views/outbox/TemplateList.vue')
        },
        {
            path: '/dictionary/outboxTemplate/edit',
            name: 'Outbox template edit',
            component: () => import('./views/outbox/EditCard.vue')
        },

        //Dictionaries
        {
            path: '/dictionary/overview',
            name: 'Dictionaries overview',
            component: () => import('./views/dictionary/Overview.vue')
        },
        {
            path: '/dictionary/standardErrands',
            name: 'Dictionary errands',
            component: () => import('./views/dictionary/StandardErrands.vue')
        },
        {
            path: '/dictionary/standardReportsByErrands',
            name: 'Dictionary reports',
            component: () => import('./views/dictionary/StandardReportsByErrands.vue')
        },
        {
            path: '/dictionary/documentTypes',
            name: 'Dictionary document types',
            component: () => import('./views/dictionary/DocumentTypes.vue')
        },
        {
            path: '/dictionary/documentTemplates',
            name: 'Dictionary document templates',
            component: () => import('./views/dictionary/DocumentTemplates.vue')
        },
        {
            path: '/dictionary/identityDocumentTypes',
            name: 'Dictionary identity document types',
            component: () => import('./views/dictionary/IdentityDocumentTypes.vue')
        },
        {
            path: '/dictionary/patentAttorneys',
            name: 'Dictionary patent attorneys',
            component: () => import('./views/dictionary/PatentAttorneys.vue')
        },
        {
            path: '/dictionary/patentAttorneyFirms',
            name: 'Dictionary patent attorney firms',
            component: () => import('./views/dictionary/PatentAttorneyFirms.vue')
        },
        {
            path: '/dictionary/typicalTexts',
            name: 'Dictionary typical texts',
            component: () => import('./views/dictionary/TypicalTexts.vue')
        },
        {
            path: '/dictionary/media',
            name: 'Dictionary media',
            component: () => import('./views/dictionary/Media.vue')
        },
        {
            path: '/dictionary/refuseRegistrationReasons',
            name: 'Dictionary refuse registration reasons',
            component: () => import('./views/dictionary/RefuseRegistrationReasons.vue')
        },
        {
            path: '/dictionary/relationshipsTypes',
            name: 'Dictionary relationships types',
            component: () => import('./views/dictionary/RelationshipsTypes.vue')
        },
        {
            path: '/dictionary/searchReportComments',
            name: 'Dictionary search report comments',
            component: () => import('./views/dictionary/SearchReportComments.vue')
        },
        {
            path: '/dictionary/actionsNomenclature',
            name: 'Dictionary actions nomenclature',
            component: () => import('./views/dictionary/ActionsNomenclature.vue')
        },
        {
            path: '/dictionary/countries',
            name: 'Dictionary countries',
            component: () => import('./views/dictionary/Countries.vue')
        },
        {
            path: '/dictionary/regions',
            name: 'Dictionary regions',
            component: () => import('./views/dictionary/Regions.vue')
        },
        {
            path: '/dictionary/subjects',
            name: 'Dictionary subjects',
            component: () => import('./views/dictionary/Subjects.vue')
        },
        {
            path: '/dictionary/incorporationForm',
            name: 'Dictionary incorporation form',
            component: () => import('./views/dictionary/IncorporationForm.vue')
        },
        {
            path: '/dictionary/goodsAndServices',
            name: 'Dictionary goods and services',
            component: () => import('./views/dictionary/GoodsAndServices.vue')
        },
        {
            path: '/dictionary/documentSubtypes',
            name: 'Dictionary document subtypes',
            component: () => import('./views/dictionary/DocumentSubtypes.vue')
        },
        {
            path: '/dictionary/deliveryMethods',
            name: 'Dictionary delivery methods',
            component: () => import('./views/dictionary/DeliveryMethods.vue')
        },
        {
            path: '/dictionary/correspondents',
            name: 'Dictionary correspondents',
            component: () => import('./views/dictionary/Correspondents.vue')
        },
        {
            path: '/dictionary/affairsNomenclature',
            name: 'Dictionary affairs nomenclature',
            component: () => import('./views/dictionary/AffairsNomenclature.vue')
        },
        {
            path: '/dictionary/workingDaysCalendar',
            name: 'Dictionary working days calendar',
            component: () => import('./views/dictionary/WorkingDaysCalendar.vue')
        },
        {
            path: '/dictionary/workingDaysCalendar/eventsList',
            name: 'Dictionary events list',
            component: () => import('./views/dictionary/EventsList.vue')
        },

        // Forms
        {
            path: '/forms/overview',
            name: 'Forms overview',
            component: () => import('./views/forms/Overview.vue')
        },
        {
            path: '/outbox/send',
            name: 'Outgoing mail sending',
            component: () => import('./views/forms/OutgoingMailSending.vue')
        },
        {
            path: '/journal/outbox/sent',
            name: 'Outgoing mail sending log',
            component: () => import('./views/forms/OutgoingMailSendingLog.vue')
        },
        {
            path: '/journal/outbox/all',
            name: 'Outgoing Journal',
            component: () => import('./views/forms/OutgoingJournal.vue')
        },
        {
            path: "/journal/outbox/separation",
            name: 'Outgoing separation',
            component: () => import('./views/forms/OutgoingSeparation.vue')
        },
        {
            path: "/outbox/handbook",
            name: 'Text block and footnote reference',
            component: () => import('./views/forms/TextBlockAndFootnoteReference.vue')
        },
    ]
});
