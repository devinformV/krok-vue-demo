import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import '@openfonts/pt-sans_cyrillic/index.css';
import '@mdi/font/css/materialdesignicons.min.css';
import './stylus/main.styl';

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
