# SUPD frontend
The main frontend part of the SUPD FIPS project.
Runs in iframe of portlet on Rospatent-online portal.

All changes commits to the `develop` branch. Separated branches must be created fot long-term tasks and named by task code. For example, `SUPD-66` 

Commit comment needs to be started with task number.
For example: `SUPD-66 Comment tab UI`

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### сборка docker образа
туториал от Vue https://ru.vuejs.org/v2/cookbook/dockerize-vuejs-app.html


сборка
```
docker build -t supd-frontend -f docker/Dockerfile .
``` 

запуск контейнера
```
docker run -it -p 8080:8080 --rm --name frontend supd-frontend:latest
```